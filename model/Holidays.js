const mongoose = require("mongoose");
const moment = require("moment");
const DATE_FORMAT = "YYYY-MM-DD";

const Schema = mongoose.Schema;

const HolidaySchema = new Schema({
  name: { type: String, required: true },
  description: String,
  date: { type: Date, required: true },
  country: { type: String, required: true }
});

/**
 * Returns list of national holidays between 2 dates for a specific country
 * 
 * @param {any} startDate 
 * @param {any} endDate 
 * @param {any} country 
 * @returns {Array}
 */
HolidaySchema.statics.getHolidays = (startDate, endDate, country) => {
  return Holiday.find({
    date: { $gte: new Date(startDate), $lte: new Date(endDate) },
    country: country
  })
    .sort({ date: 1 })
    .lean()
    .exec();
};

HolidaySchema.statics.getFormattedHolidays = async (
  startDate,
  endDate,
  country
) => {
  const holidayObjects = await Holiday.find({
    date: { $gte: new Date(startDate), $lte: new Date(endDate) },
    country: country
  })
    .sort({ date: 1 })
    .lean()
    .exec();

  return holidayObjects.map(x => {
    return moment(x.date).format(DATE_FORMAT);
  });
};

HolidaySchema.statics.getAll = (limit = 10) => {
  return Holiday.find({})
    .sort({ date: 1 })
    .lean()
    .limit(limit)
    .exec();
};

const Holiday = mongoose.model("Holiday", HolidaySchema);
module.exports = Holiday;
