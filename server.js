"use strict";

const express = require("express");
const http = require("http");
const helmet = require("helmet");
const winston = require("winston");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const httpLogger = require("morgan");

const config = require("./config");
const MainController = require("./controllers/MainController");
const PromiseWrapper = require("./helpers/promise-wrapper");

mongoose.Promise = global.Promise;
mongoose.connect(config.mongoUrl, {
  useMongoClient: true
});
mongoose.connection.on("error", () => {
  winston.error(
    "MongoDB Connection Error. Please make sure that MongoDB is running."
  );
  process.exit(1);
});

winston.configure({
  transports: [
    new winston.transports.Console({
      level: config.logLevel,
      timestamp: () => new Date().toString(),
      colorize: true
    })
  ]
});

require("./helpers/holidays-seeder")();
const app = express();
// app.use(httpLogger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(helmet());

app.post("/api/v1/work-events", PromiseWrapper(MainController.generateEvents));

const server = http.createServer(app);
server.listen(config.port);

server.on("error", error => {
  winston.error("Could not start Cloudator: ", error);
  process.exit(1);
});

server.on("listening", () => {
  winston.info(`Cloudator server started on http://localhost:${config.port}.`);
});

module.exports = server;
