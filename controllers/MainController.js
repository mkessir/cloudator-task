"use strict";

const winston = require("winston");
const Joi = require("joi");
const { ValidationError } = require("../helpers/Errors");
const WorkEventsGenerator = require("../event-generator");
const Holidays = require("../model/Holidays");

const SUPPORTED_COUNTRIES = new Set(["EST"]);

const requestSchema = Joi.object({
  employee: Joi.string().required(),
  startDate: Joi.string().required(),
  endDate: Joi.string().required(),
  country: Joi.string().required(),
  holidays: Joi.array().items(Joi.string())
});

exports.generateEvents = async (req, res, next) => {
  const validationResult = Joi.validate(req.body, requestSchema, {
    allowUnknown: true,
    abortEarly: false
  });

  if (validationResult.error) {
    return res.status(400).json({
      code: "InvalidParameters",
      message: validationResult.error.toString()
    });
  }

  if (!SUPPORTED_COUNTRIES.has(req.body.country)) {
    return res.status(400).json({
      code: "InvalidParameters",
      message: `Country ${req.body.country} Not supported`
    });
  }

  let config = {
    employee: req.body.employee,
    startDate: req.body.startDate,
    endDate: req.body.endDate,
    country: req.body.country,
    employeeHolidays: req.body.holidays
  };

  try {
    const bankHolidays = await Holidays.getFormattedHolidays(
      config.startDate,
      config.endDate,
      config.country.toUpperCase()
    );

    const params = Object.assign(config, { bankHolidays });
    const workDays = WorkEventsGenerator(params);

    res.json({
      startDate: config.startDate,
      endDate: config.endDate,
      employee: config.employee,
      workEvents: workDays,
      bankHolidays
    });
  } catch (error) {
    errorHandler(error, res);
  }
};

function errorHandler(error, res) {
  if (error instanceof ValidationError) {
    res.status(400).json({
      code: "InvalidParameters",
      message: error.message
    });
  } else {
    winston.error(error);
    res.status(500).json({ code: "ServerError", message: "Server Error" });
  }
}
