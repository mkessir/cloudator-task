const { expect } = require("chai");
const WorkEventsGenerator = require("../event-generator");

describe("Work Event Generator", () => {
  describe("Should throw error", () => {
    it("when no parameter object provided", () => {
      expect(() => {
        WorkEventsGenerator();
      }).to.throw(Error, "parameter object not provided");
    });

    it("when start date not provided", () => {
      expect(() => {
        WorkEventsGenerator({
          endDate: "2010-12-13",
          bankHolidays: ["2010-11-16", "2010-11-18"],
          employeeHolidays: ["2010-11-18"]
        });
      }).to.throw(Error, "startDate not provided");
    });

    it("when end date not provided", () => {
      expect(() => {
        WorkEventsGenerator({
          startDate: "2010-12-13",
          bankHolidays: ["2010-11-16", "2010-11-18"],
          employeeHolidays: ["2010-11-18"]
        });
      }).to.throw(Error);
    });

    it("when bankHolidays is not an array", () => {
      expect(() => {
        WorkEventsGenerator({
          startDate: "2010-12-13",
          endDate: "2010-12-13",
          bankHolidays: {},
          employeeHolidays: ["2010-11-18"]
        });
      }).to.throw(Error);
    });

    it("when employeeHolidays is not an array", () => {
      expect(() => {
        WorkEventsGenerator({
          startDate: "2010-12-13",
          endDate: "2010-12-13",
          bankHolidays: ["2010-11-16", "2010-11-18"],
          employeeHolidays: {}
        });
      }).to.throw(Error);
    });

    it("when startDate is not properly formatted", () => {
      expect(() => {
        WorkEventsGenerator({
          startDate: "201m0-12-13",
          endDate: "2010-12-11"
        });
      }).to.throw(Error, "provided startDate is invalid");
    });

    it("when period is invalid", () => {
      expect(() => {
        WorkEventsGenerator({
          startDate: "2010-12-13",
          endDate: "2010-12-11"
        });
      }).to.throw(Error, "Invalid period");
    });
  });

  describe("Valid parameters", () => {
    it("should return array when all params are provided", () => {
      const workEvents = WorkEventsGenerator({
        startDate: "2010-11-13",
        endDate: "2010-12-13",
        bankHolidays: ["2010-11-16", "2010-11-18"],
        employeeHolidays: ["2010-11-18"]
      });
      expect(workEvents).to.be.instanceOf(Array);
    });

    it("bank holidays not provided", () => {
      const workEvents = WorkEventsGenerator({
        startDate: "2010-12-10",
        endDate: "2010-12-13",
        employeeHolidays: ["2010-11-18"]
      });
      expect(workEvents).to.be.instanceOf(Array);
      expect(workEvents.length).to.be.above(0);
    });

    it("employee holidays not provided", () => {
      const workEvents = WorkEventsGenerator({
        startDate: "2010-11-13",
        endDate: "2010-12-13"
      });
      expect(workEvents).to.be.instanceOf(Array);
      expect(workEvents.length).to.be.above(0);
    });

    it("should return 5 days from 2017-09-18 to 2017-09-22", () => {
      const workEvents = WorkEventsGenerator({
        startDate: "2017-09-18",
        endDate: "2017-09-22"
      });
      expect(workEvents).to.be.instanceOf(Array);
      expect(workEvents.length).to.equal(5);
    });

    it("should return 5 days from 2017-09-18 to 2017-09-24", () => {
      const workEvents = WorkEventsGenerator({
        startDate: "2017-09-18",
        endDate: "2017-09-24"
      });
      expect(workEvents).to.be.instanceOf(Array);
      expect(workEvents.length).to.equal(5);
    });

    it("should return 9 days from 2017-09-18 to 2017-09-28", () => {
      const workEvents = WorkEventsGenerator({
        startDate: "2017-09-18",
        endDate: "2017-09-28"
      });
      expect(workEvents).to.be.instanceOf(Array);
      expect(workEvents.length).to.equal(9);
    });

    it("should return 7 days from 2017-09-18 to 2017-09-28 minus 2 bank holidays", () => {
      const workEvents = WorkEventsGenerator({
        startDate: "2017-09-18",
        endDate: "2017-09-28",
        bankHolidays: ["2017-09-19", "2017-09-20"]
      });
      expect(workEvents).to.be.instanceOf(Array);
      expect(workEvents.length).to.equal(7);
    });

    it("should return 6 days from 2017-09-18 to 2017-09-28 minus 3 employee holidays", () => {
      const workEvents = WorkEventsGenerator({
        startDate: "2017-09-18",
        endDate: "2017-09-28",
        employeeHolidays: ["2017-09-19", "2017-09-20", "2017-09-22"]
      });
      expect(workEvents).to.be.instanceOf(Array);
      expect(workEvents.length).to.equal(6);
    });


    it("should return 0 days from 2017-10-07 to 2017-10-08 (Saturday, Sunday)", () => {
      const workEvents = WorkEventsGenerator({
        startDate: "2017-10-07",
        endDate: "2017-10-08",
        employeeHolidays: []
      });
      expect(workEvents).to.be.instanceOf(Array);
      expect(workEvents.length).to.equal(0);
    });

    it("should return 1 day from 2017-10-06 to 2017-10-06 (Same day, not weekend)", () => {
      const workEvents = WorkEventsGenerator({
        startDate: "2017-10-06",
        endDate: "2017-10-06",
        employeeHolidays: []
      });
      expect(workEvents).to.be.instanceOf(Array);
      expect(workEvents.length).to.equal(1);
    });

    it("should return 0 day from 2017-10-07 to 2017-10-07 (Same day, but weekend)", () => {
      const workEvents = WorkEventsGenerator({
        startDate: "2017-10-07",
        endDate: "2017-10-07",
        employeeHolidays: []
      });
      expect(workEvents).to.be.instanceOf(Array);
      expect(workEvents.length).to.equal(0);
    });
  });
});
