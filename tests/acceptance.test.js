const server = require("../server");
const { expect } = require("chai");
const request = require("supertest")(server);

const API_URL = "/api/v1/work-events";

const validConfig = {
  employee: "Kessir Adjaho",
  startDate: "2017-10-10",
  endDate: "2017-10-15",
  country: "EST",
  holidays: ["2017-10-10"]
};
const noCountryConfig = {
  employee: "Kessir Adjaho",
  startDate: "2017-10-10",
  endDate: "2017-10-15",
  holidays: ["2017-10-10"]
};
const wrongCountryConfig = {
  employee: "Kessir Adjaho",
  startDate: "2017-10-10",
  endDate: "2017-10-15",
  holidays: ["2017-10-10"],
  country: "FRA",
};

const noDateConfig = {
  employee: "Kessir Adjaho",
  endDate: "2017-10-15",
  holidays: ["2017-10-10"]
};

describe("Acceptance tests", () => {
  describe("Should return error", () => {
    it("when given config with no start dates", async () => {
      const { body } = await request
        .post(API_URL)
        .send(noDateConfig)
        .expect(400)
        .expect("Content-Type", "application/json; charset=utf-8");
      expect(body.code).to.be.equal("InvalidParameters");
    });

    it("when given config json with no country", async () => {
      const { body } = await request
        .post(API_URL)
        .send(noCountryConfig)
        .expect(400)
        .expect("Content-Type", "application/json; charset=utf-8");
      expect(body.code).to.be.equal("InvalidParameters");
    });

    it("when given config json with non-supported country", async () => {
      const { body } = await request
        .post(API_URL)
        .send(wrongCountryConfig)
        .expect(400)
        .expect("Content-Type", "application/json; charset=utf-8");
      expect(body.code).to.be.equal("InvalidParameters");
    });
  });

  describe("Should return work events", () => {
    it("when given valid config json", async () => {
      const { body } = await request
        .post(API_URL)
        .send(validConfig)
        .expect(200)
        .expect("Content-Type", "application/json; charset=utf-8");

      expect(body.employee).to.be.equal(validConfig.employee);
      expect(body.startDate).to.be.equal(validConfig.startDate);
      expect(body.endDate).to.be.equal(validConfig.endDate);
      expect(body.workEvents).to.be.an("array");
      expect(body.bankHolidays).to.be.an("array");
    });
  });
});
