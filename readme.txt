Requires:
- Nodejs 7.6+ (for async/await)
- Mongodb (for storing National Holidays locally)


How to run the app:
- npm install
- make sure mongodb is running
- npm start

How to run tests:
- All tests: npm test
- Unit tests: npm run test:unit
- Acceptance test: npm run test:acceptance



API Documentation
-----------------

POST request to 'http://localhost:3333/api/v1/work-events' with a payload of the following format:
{
  "employee": "Kessir Adjaho",
  "startDate": "2017-02-20",
  "endDate": "2017-02-28",
  "holidays": ["2017-02-22"],
  "country": "EST"                  // 3 letter ISO country code
}

Response format:
{
    "startDate": "2017-02-20",
    "endDate": "2017-02-28",
    "employee": "Kessir Adjaho",
    "workEvents": [
        "2017-02-20",
        "2017-02-21",
        "2017-02-23",
        "2017-02-27",
        "2017-02-28"
    ],
    "bankHolidays": [
        "2017-02-24"
    ]
}