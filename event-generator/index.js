"use strict";
const { ValidationError } = require("../helpers/Errors");
const moment = require("moment");
const DATE_FORMAT = "YYYY-MM-DD";

/**
 * Assign the project to an employee.
 * @param {Object} params - An object containing all parameters
 * @param {string} params.startDate - The name of the employee.
 * @param {string} params.endDate - The name of the employee.
 * @param {string} params.bankHolidays - The name of the employee.
 * @param {string} params.employeeHolidays - The employee's department.
 * @return {Array} Array containing list of WorkDays
 */
module.exports = params => {
  const validParams = validateAndSanitizeParams(params);

  const holidaysSet = new Set(
    validParams.bankHolidays.concat(validParams.employeeHolidays)
  );
  return getWorkDaysExcludingHolidays(
    validParams.startDate,
    validParams.endDate,
    holidaysSet
  );
};

/**
 * 
 * 
 * @param {any} params 
 * @returns {Object} sanitized parameter object
 * @throws Errors
 */
function validateAndSanitizeParams(params) {
  if (!params) {
    throw new ValidationError("parameter object not provided");
  }
  if (!params.startDate) {
    throw new ValidationError("startDate not provided");
  }
  if (!params.endDate) {
    throw new ValidationError("endDate not provided");
  }
  if (!moment(params.startDate, DATE_FORMAT, true).isValid()) {
    throw new ValidationError("provided startDate is invalid");
  }
  if (!moment(params.endDate, DATE_FORMAT, true).isValid()) {
    throw new ValidationError("provided endDate is invalid");
  }

  if (moment(params.endDate).isBefore(params.startDate)) {
    throw new ValidationError("Invalid period");
  }

  params.bankHolidays = params.bankHolidays || [];
  params.employeeHolidays = params.employeeHolidays || [];

  if (!Array.isArray(params.bankHolidays)) {
    throw new ValidationError("bankHolidays is not an array");
  }
  if (!Array.isArray(params.employeeHolidays)) {
    throw new ValidationError("employeeHolidays is not an array");
  }
  return params;
}

/**
 * Generates list of work days between startDate and endDate inclusive excluding dates in excludedDates
 * @param {string} startDate
 * @param {string} endDate
 * @param {Array} holidays
 * @returns {*|Array|Array.<T>}
 */
function getWorkDaysExcludingHolidays(
  startDate,
  endDate,
  holidays = new Set()
) {
  const days = getWorkDays(startDate, endDate);
  return days.filter(notInSet(holidays));
}

/**
 * Generates list of work days between startDate and endDate inclusive (excluding weekends of course)
 * @param startDate
 * @param endDate
 * @returns {Array}
 */
function getWorkDays(startDate, endDate) {
  let currentDate = startDate;
  const workDays = [];

  if (isNotWeekend(currentDate)) workDays.push(currentDate);

  while (currentDate !== endDate) {
    currentDate = moment(currentDate)
      .add(1, "days")
      .format(DATE_FORMAT);

    if (isNotWeekend(currentDate)) workDays.push(currentDate);
  }
  
  return workDays;
}

/**
 * Is #currentDate a weekend?
 * 
 * @param {string} date 
 * @returns {Boolean}
 */
function isNotWeekend(date) {
  return moment(date).isoWeekday() !== 6 && moment(date).isoWeekday() !== 7;
}

function notInSet(holidays) {
  return x => !holidays.has(x);
}
