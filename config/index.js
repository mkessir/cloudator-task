require("dotenv").config();
const nodeEnv = process.env.NODE_ENV || "development";

const defaultConfig = {
  mongoUrl: process.env.MONGO_URL || "mongodb://127.0.0.1:27017/holidays",
  port: process.env.PORT || "3333",
  logLevel: process.env.LOG_LEVEL || "debug"
};

const testConfig = {
  mongoUrl: process.env.MONGO_URL || "mongodb://127.0.0.1:27017/holidays",
  port: "3335",
  logLevel: "warn"
};

const config = nodeEnv === "test" ? testConfig : defaultConfig;
module.exports = config;
