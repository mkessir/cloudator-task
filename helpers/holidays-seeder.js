"use strict";
const winston = require("winston");
const Holidays = require("../model/Holidays");
const initialData = require("./holidays.json");

module.exports = async () => {
  winston.info("holiday data seeder...");
  const holidays = await Holidays.getAll();
  if (holidays.length > 0) return;

  for (let holiday of initialData) {
    await new Holidays(holiday).save().catch(err => {
      winston.error(err);
    });
  }
};
