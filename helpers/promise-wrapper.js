/**
 * Transforms a promise into the type of function expected by express routes
 * @param fn
 * @returns {function(*=, *=, *=)}
 */
const promiseWrapper = fn => {
  return (req, res, next) => {
    Promise.resolve(fn(req, res, next)).catch(next);
  };
};
module.exports = promiseWrapper;
